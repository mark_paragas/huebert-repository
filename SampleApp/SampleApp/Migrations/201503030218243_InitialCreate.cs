namespace SampleApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProdId = c.Int(nullable: false, identity: true),
                        ProdName = c.String(),
                        ProdPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        isActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProdId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Products");
        }
    }
}
