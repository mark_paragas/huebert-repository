namespace SampleApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProdUId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "ProdUId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "ProdUId");
        }
    }
}
