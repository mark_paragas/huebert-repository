﻿Ext.define('AM.store.Products', {
    extend: 'Ext.data.Store',
    model: 'AM.model.Product',
    autoLoad: true,
    pageSize: 4,

    proxy: {
        type: 'ajax',
        api: {
            read: '/api/Product/GetProduct'
        },
        reader: {
            type: 'json',
            root: 'Products',
            successProperty: 'success',
            totalProperty: 'total'
        }
    }
});


