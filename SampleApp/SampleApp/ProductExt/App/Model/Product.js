﻿Ext.define('AM.model.Product', {
    extend: 'Ext.data.Model',
    fields: [ 'ProdId', 'ProdUId', 'ProdName', 'ProdPrice', 'isActive' ]
});