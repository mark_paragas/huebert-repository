﻿Ext.define('AM.controller.Product', {
    extend: 'Ext.app.Controller',
    stores: ['Products'],
    models: ['Product'],

    views: [
        'Product.List',
        'Product.Edit',
        'Product.Create'
    ],

    init: function () {
        this.control({
            'userlist': {
                itemdblclick: this.editUser
            },
            'userlist button[action=saveProduct]': {
                click: this.create
            },
            'useredit button[action=save]': {
                click: this.updateUser

            },
            'useredit button[action=delete]': {
                click: this.deleteUser
            },
            'create button[action=save]': {
                click: this.save
            }
        });
    },
    
    create: function (grid, record) {
        var view = Ext.widget('create');
        view.down('form').loadRecord(record);
    },
    editUser: function (grid, record) {
        var view = Ext.widget('useredit');
        view.down('form').loadRecord(record);
    },
    save: function (button) {
        
        var win = button.up('window'),
            form = win.down('form');
        var prodId = form.getValues().ProdId;
        var prodUId = form.getValues().ProdUId;
        var proName = form.getValues().ProdName;
        var prodPrice = form.getValues().ProdPrice;
        var isActive = form.getValues().isActive;
        
        Ext.Ajax.request({
            method: 'POST',
            url: '/api/Product/SaveProduct/',
            contentType: 'application/json',
            dataType: 'json',
            jsonData: '{ "ProdId": ' + 0 + ', "ProdUId": "' + prodUId + '", "ProdName": "' + proName + '", "ProdPrice": "' + prodPrice + '", "isActive":' + isActive + ' }',
            success: function () {
                Ext.getCmp('ProductGrid').getStore().load();
            },
            failure: function () {
                alert('Failed to Save.');
            }
        });
        win.close();
    },
    updateUser: function (button) {
        
        var win = button.up('window'),
            form = win.down('form');
        var prodId = form.getValues().ProdId;
        var prodUId = form.getValues().ProdUId;
        var proName = form.getValues().ProdName;
        var prodPrice = form.getValues().ProdPrice;
        var isActive = form.getValues().isActive;

        Ext.Ajax.request({
            method: 'PUT',
            url: '/api/Product/UpdateProduct/',
            contentType: 'application/json',
            dataType: 'json',
            jsonData: '{ "ProdId": ' + prodId + ', "ProdUId": "' + prodUId + '", "ProdName": "' + proName + '", "ProdPrice": "' + prodPrice + '", "isActive":' + isActive + ' }',
            success: function () {
                Ext.getCmp('ProductGrid').getStore().load();
            },
            failure: function () {
                alert('Failed to Update.');
            }
        });
        win.close();
    },
    deleteUser: function (button) {
        var win = button.up('window'),
            form = win.down('form');
        var prodId = form.getValues().ProdId;

        Ext.Ajax.request({
            method: 'DELETE',
            url: '/api/Product/DeleteProduct?prodId=' + prodId,
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(),
            success: function () {
                Ext.getCmp('ProductGrid').getStore().load();
            },
            failure: function () {
                alert('Failed to Delete.');
            }
        });
        win.close();
    }
});