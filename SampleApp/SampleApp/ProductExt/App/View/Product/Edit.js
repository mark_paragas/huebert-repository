﻿Ext.define('AM.view.Product.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.useredit',

    title: 'Edit Product',
    layout: 'fit',
    autoShow: true,

    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'ProdId',
                        fieldLabel: 'Id'
                    },
                    {
                        xtype: 'textfield',
                        name: 'ProdUId',
                        fieldLabel: 'Product Id',
                        allowBlank: false,
                        msgTarget: 'side',
                        blankText: 'Product Id is required.'
                        
                    },
                    {
                        xtype: 'textfield',
                        name: 'ProdName',
                        fieldLabel: 'Product Name',
                        allowBlank: false,
                        msgTarget: 'side',
                        blankText: 'Product Name is required.'
                    },
                    {
                        xtype: 'textfield',
                        name: 'ProdPrice',
                        fieldLabel: 'Product Price',
                        allowBlank: false,
                        msgTarget: 'side',
                        blankText: 'Price is required.'
                    },
                    {
                        xtype: 'textfield',
                        name: 'isActive',
                        fieldLabel: 'Is Active'
                    }
                ]
            }
        ];

        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Delete',
                action: 'delete'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];

        this.callParent(arguments);
    }
});