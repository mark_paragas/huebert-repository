﻿Ext.define('AM.view.Product.List', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.userlist',
    id: 'ProductGrid',
    title: 'Product Record',
    store: 'Products',

    dockedItems: [{
                xtype: 'pagingtoolbar',
                store: 'Products',
                dock: 'bottom',
                pageSize: 4
            }],

    initComponent: function () {
        this.columns = [
            { header: 'Id', dataIndex: 'ProdId', flex: 1 },
            { header: 'Product Id', dataIndex: 'ProdUId', flex: 1 },
            { header: 'Product Name', dataIndex: 'ProdName', flex: 1 },
            { header: 'Product Price', dataIndex: 'ProdPrice', flex: 1 },
            { header: 'Is Active', dataIndex: 'isActive', flex: 1 }
        ];

        this.buttons = [
            {
                text: 'Add Product',
                action: 'saveProduct'
            }
        ];
 
        this.callParent(arguments);
    }

});


//Ext.create('Ext.grid.Panel', {
//    store: 'Products',
//    columns: [
//            { header: 'Id', dataIndex: 'ProdId', flex: 1 },
//            { header: 'Product Id', dataIndex: 'ProdUId', flex: 1 },
//            { header: 'Product Name', dataIndex: 'ProdName', flex: 1 },
//            { header: 'Product Price', dataIndex: 'ProdPrice', flex: 1 },
//            { header: 'Is Active', dataIndex: 'isActive', flex: 1 }
//    ],
//    dockedItems: [{
//        xtype: 'pagingtoolbar',
//        store: userStore,   // same store GridPanel is using
//        dock: 'bottom',
//        displayInfo: true
//    }]
//});