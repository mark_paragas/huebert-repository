﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleApp.Models
{
    
    public class UsersResponse : Response
    {
        public IEnumerable<Users> users { get; set; }
    }

    public class Users
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
    }
}