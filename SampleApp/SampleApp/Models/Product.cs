﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SampleApp.Models
{
    public class ProductResponse
    {
        public IEnumerable<Product> Products { get; set; }
    }

    public class Product
    {
        [System.ComponentModel.DataAnnotations.KeyAttribute()]
        public int ProdId { get; set; }
        public string ProdUId { get; set; }
        public string ProdName { get; set; }
        public decimal ProdPrice { get; set; }
        public bool isActive { get; set; }

    }

    public class ProductContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
    }
}