﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using SampleApp.Models;

namespace SampleApp.Controllers
{
    public class ProductController : ApiController
    {
        ProductContext db = new ProductContext();

        [HttpGet]
        public ProductResponse GetProduct()
        {
            var prod = db.Products.ToList();
            ProductResponse productRes = new ProductResponse();
            productRes.Products = prod.AsEnumerable();
            return productRes;
        }

        [HttpPost]
        public Response SaveProduct([FromBody]Product SaveProd)
        {
            var ret = new Response();
            try
            {
                Product prod = new Product() { ProdUId = SaveProd.ProdUId, ProdName = SaveProd.ProdName, ProdPrice = SaveProd.ProdPrice, isActive = SaveProd.isActive };
                db.Products.Add(prod);
                db.SaveChanges();
                ret.Result = "Insert Successful";
                return ret;
            }
            catch (Exception)
            {
                ret.Result = "Insert Failed";
                return ret;
            }
        }

        [HttpPut]
        public Response UpdateProduct([FromBody]Product UpdateProd)
        {
            var ret = new Response();
            try
            {
                var prod = db.Products.ToList();
                var q = db.Products.First(p => p.ProdId == UpdateProd.ProdId);
                q.ProdUId = UpdateProd.ProdUId;
                q.ProdName = UpdateProd.ProdName;
                q.ProdPrice = UpdateProd.ProdPrice;
                q.isActive = UpdateProd.isActive;
                db.SaveChanges();
                
                ret.Result = "Update Successful";
                return ret;
            }
            catch (Exception)
            {
                ret.Result = "Update Failed";
                return ret;
            }
        }

        [HttpDelete]
        public Response DeleteProduct(int prodId)
        {
            var ret = new Response();
            try
            {
                var q = db.Products.First(p => p.ProdId == prodId);
                db.Products.Remove(q);
                db.SaveChanges();
                ret.Result = "Delete Successful";
                return ret;
            }
            catch (Exception)
            {
                ret.Result = "Delete Failed";
                return ret;
            }
        }
    }
}
