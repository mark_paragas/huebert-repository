﻿using SampleApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace SampleApp.Controllers
{
    public class ValuesController : ApiController
    {
        ProductContext db = new ProductContext();

        [HttpPost]
        public void SaveProduct([FromBody]Product Product)
        {
            Product prod = new Product() { ProdName = Product.ProdName, ProdPrice = Product.ProdPrice, isActive = Product.isActive };
            db.Products.Add(prod);
            db.SaveChanges();

        }

        [HttpPost]
        public List<Product> RetrieveProductById([FromBody]Product ret)
        {
            var prod = db.Products.ToList();
            var q = db.Products.Where(s => s.ProdId == ret.ProdId).ToList();

            return q;
        }

        

        //// PUT api/values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/values/5
        //public void Delete(int id)
        //{
        //}
    }
}